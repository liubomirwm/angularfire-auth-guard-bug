// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    apiKey: 'AIzaSyBj2rsYiLxN__Qbw5YvdqunDOxAJNkrzhM',
    authDomain: 'angularfire-auth-guard-bug.firebaseapp.com',
    projectId: 'angularfire-auth-guard-bug',
    storageBucket: 'angularfire-auth-guard-bug.appspot.com',
    messagingSenderId: '604712475432',
    appId: '1:604712475432:web:c7bd66b485ae1159614e1d',
  },
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
import 'zone.js/plugins/zone-error'; // Included with Angular CLI.
