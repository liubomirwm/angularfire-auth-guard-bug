import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainLoginPromptComponent } from './main-login-prompt.component';

describe('MainLoginPromptComponent', () => {
  let component: MainLoginPromptComponent;
  let fixture: ComponentFixture<MainLoginPromptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainLoginPromptComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MainLoginPromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
