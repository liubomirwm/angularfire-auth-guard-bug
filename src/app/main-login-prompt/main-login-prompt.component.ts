import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { FirebaseError } from '@angular/fire/app';
import firebase from 'firebase/compat/app';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-main-login-prompt',
  templateUrl: './main-login-prompt.component.html',
  styleUrls: ['./main-login-prompt.component.css'],
})
export class MainLoginPromptComponent {
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(
    public auth: AngularFireAuth,
    public matSnackBar: MatSnackBar,
    public router: Router,
    public currentRoute: ActivatedRoute
  ) {}

  async login() {
    try {
      await this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
      console.log('Before calling router navigate');
      let promise = await this.router.navigate(this.currentRoute.snapshot.url);
      console.log('After calling router navigate');
      console.log(promise);
      console.log('After printing promise result');
    } catch (error) {
      if (
        error instanceof FirebaseError &&
        error.code === 'auth/admin-restricted-operation'
      ) {
        this.showSnackBar('Authentication restricted', 'Dismiss');
      } else {
        console.log(error);
        this.showSnackBar('Error during authentication', 'Dismiss');
      }
    }
  }

  showSnackBar(message: string, action: string | undefined) {
    this.matSnackBar.open(message, action, {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
    });
  }
}
