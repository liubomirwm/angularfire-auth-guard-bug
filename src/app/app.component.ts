import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'my-agile-and-lean-life-frontend';

  constructor(public auth: AngularFireAuth) {}

  logout() {
    this.auth.signOut();
  }
}
